import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request } from 'express';

// nest g middleware logger --no-spec --flat

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: any, next: () => void) {
    console.log('URL:', req.url);
    next();
  }
}
