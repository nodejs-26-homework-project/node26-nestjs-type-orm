import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Query,
  Req,
  Res,
} from '@nestjs/common';
import { UserService } from './user.service';
import { Request, Response } from 'express';
import { Filter, FindAllQuery, Paging } from './dto/find-all.dto';
import { CreateUserDto } from './dto/create.dto';
import { log } from 'console';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}
  dto;
  @Get()
  findAll(@Query() query: FindAllQuery) {
    const paging: Paging = { page: query.page, pageSize: query.pageSize };
    const filter: Filter = { role: query.role };
    return this.userService.findAllWithCondition(paging, filter);
  }

  @Get('/:id')
  findById(@Param('id') id: string) {
    return this.userService.findOneWithCondition({ id });

    // Xử lý trả lỗi về cho client
    // throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    // throw new NotFoundException('user not found');
  }

  // Override status code bằng res của express
  // @Post('/users')
  // create(@Res() res: Response) {
  //   res.status(201).json('Create user success');
  // }

  // Override status code bằng HttpCode decorator
  @Post()
  @HttpCode(201)
  create(@Body() createUserDto: CreateUserDto) {
    if (!Object.keys(createUserDto).length) {
      throw new BadRequestException('missing user data');
    }

    return this.userService.create(createUserDto);
  }
}
