import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { User } from '../User/user.model';
import { JwtStrategy } from './strategies/jwt.strategy';

@Module({
  imports: [
    // Setup sử dụng JWT bên trong các services được quản lý bới AuthModule
    JwtModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        secret: config.get('SECRET_KEY'),
        signOptions: {
          expiresIn: '12h',
        },
      }),
      inject: [ConfigService],
    }),

    // Setup sử dụng Passport bên trong các services được quản lý bới AuthModule
    PassportModule,

    // Khai báo cho phép sử dụng User entity trong các services do AuthModule quản lý
    TypeOrmModule.forFeature([User]),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
})
export class AuthModule {}
