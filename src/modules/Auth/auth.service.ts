import {
    BadRequestException,
    Injectable,
    InternalServerErrorException,
  } from '@nestjs/common';
  import { InjectRepository } from '@nestjs/typeorm';
  import { JwtService } from '@nestjs/jwt';
  import { Repository, QueryFailedError } from 'typeorm';
  import { User } from '../User/user.model';
  import { LoginDto } from './dto';
  
  @Injectable()
  export class AuthService {
    constructor(
      @InjectRepository(User) private userRepository: Repository<User>,
      private jwtService: JwtService,
    ) {}
  
    async login({ email, password }: LoginDto) {
      try {
        const user = await this.userRepository.findOne({
          where: { email },
        });
  
        if (!user) {
          throw new BadRequestException('email or password invalid');
        }
  
        // if(user.password === password)
  
        return {
          accessToken: this.jwtService.sign({ email }),
          expiresIn: 60 * 60 * 12,
        };
      } catch (error) {
        if (error instanceof QueryFailedError) {
          throw new InternalServerErrorException();
        }
        throw error;
      }
    }
  }
  